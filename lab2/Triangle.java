import java.util.Scanner;


public class Triangle{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Input number for the height of triangle : ");
		int height = input.nextInt();
		System.out.println("Okey, the height of the triangle is : "+height);
		
		int max = 2*height - 1;
		for (int i = 0; i < height;i++){
			int n_stars = 2*i+1; 
			int n_spaces = (max - n_stars)/2;
			for (int j = 0; j <n_spaces; j++){
				System.out.print(" ");
			}	
			for (int j = 0; j<n_stars;j++){
				System.out.print("*");
			}	
			System.out.println();
		}
	}
}
