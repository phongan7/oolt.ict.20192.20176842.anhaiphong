import java.util.Random;
import java.util.Arrays;
class Matrix{
	private int height = 0;
	private int width = 0;
	private int[][] val = new int[50][50]; 
	void random(int _height, int _width){
		
		if (_height>50 || _width >50) {
			System.out.println("Invalid size");
			return;
		}
		
		height = _height;
		width = _width;
		Random rand = new Random();
		for (int i = 0; i < height;i++){
			for (int j = 0; j < width;j++){
				val[i][j] =  rand.nextInt(1000);		
			}
		}
	}
	void zeros(int _height, int _width){
		
		if (_height>50 || _width >50) {
			System.out.println("Invalid size");
			return;
		}
		
		height = _height;
		width = _width;
		for (int i = 0; i < height;i++){
			for (int j = 0; j < width;j++){
				val[i][j] =  0;		
			}
		}
	}
	public void show(){
		for (int i = 0;i<height;i++){
			for (int j = 0;j<width;j++){
				System.out.printf("|%5d", val[i][j]);
			}
			System.out.print("|");
			System.out.println();
		}
	}
	Matrix sum(Matrix m2){
		if (height!= m2.height || width != m2.width){
			System.out.println("Unable to sum 2 different-sized matrices, return 1x1 zero");
			Matrix x = new Matrix();
			x.zeros(1,1);
			return x;
		}
		Matrix res = new Matrix();
		res.zeros(height, width);
		for (int i = 0; i<height;i++){
			for (int j = 0; j<width;j++){
				res.val[i][j] = val[i][j] + m2.val[i][j];
			}
		}
		return res;
	}
}
