package hust.soict.ictglobal.aims.order;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERED = 5;
    private static int nbOrders = 0;

    public Order(String dateOrdered) {
        if (nbOrders < MAX_LIMITED_ORDERED) {
            this.dateOrdered = dateOrdered;
            nbOrders++;
            System.out.println("Order number " + nbOrders + " is created");
        } else
            System.out.println("You have reached the limited orders, cannot add this order to the list");
    }

    private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
    private int qtyOrdered = 0;
    private String dateOrdered;

    public String getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(String dateOrdered) {
        this.dateOrdered = dateOrdered;
    }


    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered < MAX_NUMBER_ORDERED) {
            itemOrdered[qtyOrdered] = disc;
            qtyOrdered++;
        } else {
            System.out.println("Full items ordered ! Cannot add " + disc.getTitle());
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
        for (int i = 0; i < dvdList.length; i++) {
            addDigitalVideoDisc(dvdList[i]);
        }
    }

    public DigitalVideoDisc[] getItemOrdered() {
        return itemOrdered;
    }

    public void setItemOrdered(DigitalVideoDisc[] itemOrdered) {
        this.itemOrdered = itemOrdered;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
        addDigitalVideoDisc(dvd1);
        addDigitalVideoDisc(dvd2);
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered > 0) qtyOrdered--;
        else System.out.println("Cannot delete because your order is empty");
    }

    public float totalCost() {
        float sum = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            sum += itemOrdered[i].getCost();
        }
        return sum;
    }

    public void print(Order order) {
        System.out.println("***********Order************");
        System.out.println("Date: " + order.getDateOrdered());
        System.out.println("Order items: ");
        for (int i = 0; i < order.getQtyOrdered(); i++) {
            System.out.println(i + 1 + ". DVD - " + order.getItemOrdered()[i].getTitle() + " - " + order.getItemOrdered()[i].getCategory()
                    + " - " + order.getItemOrdered()[i].getDirector() + " - " + order.getItemOrdered()[i].getLength() + " : "
                    + order.getItemOrdered()[i].getCost() + "$");
        }
        System.out.println("Total cost: " + order.totalCost());
        System.out.println("****************************");
    }

    public DigitalVideoDisc getALuckyItem(){
        int i= (int) (Math.random() *this.getQtyOrdered());
        this.getItemOrdered()[i].setCost(0.0f);
        return this.getItemOrdered()[i];
    }
}
