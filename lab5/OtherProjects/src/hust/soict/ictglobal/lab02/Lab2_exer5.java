package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Lab2_exer5 {
    public static void main(String[] args) {
        int month, year;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter month: ");
        month = scanner.nextInt();
        while (month < 1 || month > 12) {
            System.out.println("Invalid month ! Please try again");
            month = scanner.nextInt();
        }
        System.out.printf("Enter year :");
        year = scanner.nextInt();
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 | month == 12)
            System.out.println("31 days");
        else if (month == 4 || month == 6 || month == 9 || month == 11)
            System.out.println("30 days");
        else {
            if (year % 4 == 0) {
                if (year % 100 != 0) System.out.println("29 days");
                else {
                    if (year % 400 == 0) System.out.println("29 days");
                    else System.out.println("28 days");
                }
            } else System.out.println("28 days");
        }
    }
}
