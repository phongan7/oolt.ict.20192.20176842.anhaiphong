package hust.soict.ictglobal.date;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Scanner;

public class MyDate {
    private int day;
    private int month;
    private int year;
    Date now = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");
    String myTime = simpleDateFormat.format(now);
    String[] mySeparatedTime = myTime.split("/");
    static Hashtable<String, Integer> myMap = new Hashtable<String, Integer>();

    public Hashtable<String, Integer> getMyMap() {
        return myMap;
    }

    public MyDate() {
        this.getMyMap().put("January", 1);
        this.getMyMap().put("February", 2);
        this.getMyMap().put("March", 3);
        this.getMyMap().put("April", 4);
        this.getMyMap().put("May", 5);
        this.getMyMap().put("June", 6);
        this.getMyMap().put("July", 7);
        this.getMyMap().put("August", 8);
        this.getMyMap().put("September", 9);
        this.getMyMap().put("October", 10);
        this.getMyMap().put("November", 11);
        this.getMyMap().put("December", 12);
        this.day = Integer.parseInt(mySeparatedTime[0]);
        this.month = Integer.parseInt(mySeparatedTime[1]);
        this.year = Integer.parseInt(mySeparatedTime[2]);
    }

    public MyDate(int day, int month, int year) {
        if (day > 0 && day < 32 && month > 0 && month < 13 && year > 0) {
            this.day = day;
            this.month = month;
            this.year = year;
        } else System.out.println("Invalid hust.soict.ictglobal.date !");
    }

    public MyDate(String represent) {
        String[] temp = represent.split(" ");
        this.day = Integer.parseInt(temp[1].substring(0, temp[1].length() - 2));
        this.year = Integer.parseInt(temp[2]);
        this.month = myMap.get((temp[0]));
    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(String month) {
        this.month = myMap.get(month);

    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void myPrint() {
        System.out.println(now);
    }

    public void print() {
        System.out.println(myTime);
    }

    public void accept() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a string following mm dd yyyy form :");
        String represent = scanner.nextLine();
        MyDate tempDate = new MyDate(represent);
        System.out.printf("Variable myDate after accept function : %d/%d/%d\n",
                tempDate.getDay(), tempDate.getMonth(), tempDate.getYear());
    }

    public void anotherPrint(){
        Scanner scanner= new Scanner(System.in);
        int choice;
        System.out.println("Choose a format to print the current day :");
        System.out.println("1. dd/mm/yyyy");
        System.out.println("2. yy-mm-dd");
        System.out.print("Your choice :"); choice=scanner.nextInt();
        if(choice==1){
            System.out.println(myTime);
        }
        else {
            System.out.printf("%d-%d-%d",Integer.parseInt(mySeparatedTime[2]),Integer.parseInt(mySeparatedTime[1]),
                    Integer.parseInt(mySeparatedTime[0]));
        }
    }

}
