package hust.soict.ictglobal.garbage;

public class NoGarbage {
    public static void main(String[] args) {
        String s = "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 1000000; i++)
            sb.append("a");
        s=sb.toString();
    }
}
