import java.util.*;
import java.lang.Math;
import javax.swing.JOptionPane;

public class Calculate{

	public static void main(String[] args){
		String s;
		s = JOptionPane.showInputDialog(null,"Input the the first number", JOptionPane.INFORMATION_MESSAGE);

		// double x = scan.nextDouble();
		// double y = scan.nextDouble();
		double x = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input the the second number", JOptionPane.INFORMATION_MESSAGE);
		double y = Double.parseDouble(s);
		scan.close();

		JOptionPane.showMessageDialog(null, x + " and " + y, "Input : ", JOptionPane.INFORMATION_MESSAGE);

		double sum = x+y;
		JOptionPane.showMessageDialog(null, sum, "Sum : ", JOptionPane.INFORMATION_MESSAGE);

		double diff = Math.abs(x-y);
		JOptionPane.showMessageDialog(null, diff, "Difference : ", JOptionPane.INFORMATION_MESSAGE);

		double product = x*y;
		JOptionPane.showMessageDialog(null, product, "Product : ", JOptionPane.INFORMATION_MESSAGE);

		if (y == 0){
			JOptionPane.showMessageDialog(null, "Cannot be divided by zero");
		}
		else{
			double quotient = x/y;
			JOptionPane.showMessageDialog(null, quotient, "Quotient : ", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}