public class Aims{
	
	public static void main(String[] args){
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		anOrder.addDigitalVideoDisc(dvd3);

		System.out.print("Total Cost is : ");
		System.out.println(anOrder.totalCost());
		System.out.print("Number of elements : ");
		System.out.println(anOrder.getQty());
		
		System.out.println("Test remove elements: 1");
		anOrder.removeDigitalVideoDisc(dvd1);
		System.out.print("Number of elements left : ");
		System.out.println(anOrder.getQty());
		System.out.println("Check title : ");
		for (int i = 0; i< anOrder.getQty();i++){
			System.out.println(anOrder.itemsOrdered[i].getTitle());
		}
		
		System.out.println("test lab 4 : ");
		DigitalVideoDisc dvdList[] = new DigitalVideoDisc[3];
		dvdList[0] = dvd3;
		dvdList[1] = dvd2;
		dvdList[2] = dvd1;
		anOrder.addDigitalVideoDisc(dvdList);
		System.out.print("Number of elements : ");
		System.out.println(anOrder.getQty());
		
		System.out.println("continue test : ");
		anOrder.addDigitalVideoDisc(dvdList[0],dvdList[1]);
		System.out.print("Number of elements : ");
		System.out.println(anOrder.getQty());
		
		anOrder.getDateTime();
		anOrder.printOrder();
	}	
}
















